<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;

    class ExpenseModel extends Model {
        protected function getFields(): array {
            return [
                'expense_id'            => new Field((new NumberValidator())->setIntegerLength(11), false),
                
                'intervention_name'     => new Field((new StringValidator)->setMaxLength(255) ),
                'price'                 => new Field((new NumberValidator())->setIntegerLength(11) ),
                'vehicle_id'            => new Field((new NumberValidator())->setIntegerLength(11) ),
            ];
        }

        public function getAllByExpenseId(int $expenseId): array {
            return $this->getAllByFieldName('expense_id', $expenseId);
        }
    }
