<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;

    class FeatureModel extends Model {
        protected function getFields(): array {
            return [
                'feature_id'      => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                
                'name'          => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
            ];
        }

        public function getAllByFeatureId(int $featureId): array {
            return $this->getAllByFieldName('feature_id', $featureId);
        }
    }
