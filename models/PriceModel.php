<?php
    namespace App\Models;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class PriceModel extends Model {
        protected function getFields(): array{
            return [
                'price_id'    => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'  => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),
                
                'vehicle_id'  => new Field((new NumberValidator())->setIntegerLength(11), true),
                'price'       => new Field((new NumberValidator())->setIntegerLength(11), true)
            ];
        }

        public function getAllByVehicleId(int $vehicleId): array {
            return $this->getAllByFieldName('vehicle_id', $vehicleId);
        }  

        public function getAllByPriceId(int $priceId) {
            return $this->getByFieldName('price_id', $priceId);
        }
    }