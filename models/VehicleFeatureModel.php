<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;

    class VehicleFeatureModel extends Model {
        protected function getFields(): array {
            return [
                'vehicle_feature_id'    => new Field((new NumberValidator())->setIntegerLength(11), false),
                
                'feature_id'            => new Field((new NumberValidator())->setIntegerLength(11), true),
                'vehicle_id'            => new Field((new NumberValidator())->setIntegerLength(11), true),
                'value'                 => new Field((new StringValidator)->setMaxLength(255) ),
                
            ];
        }

        public function getAllByVehicleFeatureId(int $vehicleFeatureId): array {
            return $this->getAllByFieldName('vehicle_feature_id', $vehicleFeatureId);
        }

        public function getByUsername(string $username) {
            return $this->getByFieldName('username', $username);
        }
    }
