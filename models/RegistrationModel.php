<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class RegistrationModel extends Model {
        protected function getFields(): array {
            return [
                'registration_id'  => new Field((new NumberValidator())->setIntegerLength(11), false),
                'registred_at'     => new Field((new DateTimeValidator())->allowDate()->allowTime() ),
                'expires_at'       => new Field((new DateTimeValidator())->allowDate()->allowTime() ),
                
                'registration'     => new Field((new StringValidator)->setMaxLength(255) ),
                'vehicle_id'       => new Field((new NumberValidator())->setIntegerLength(11) )
            ];
        }

        public function getAllByRegistrationId(int $registrationId): array {
            return $this->getAllByFieldName('registration_id', $registrationId);
        }

        public function getAllByFeatureValue() :array {
            $sql = 'SELECT *  FROM registration
            INNER JOIN vehicle ON registration.vehicle_id = vehicle.vehicle_id
            INNER JOIN vehicle_feature ON vehicle.vehicle_id = vehicle_feature.vehicle_id
            INNER JOIN feature ON vehicle_feature.feature_id = feature.feature_id 
            WHERE  feature.feature_id = 1';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([]);
            
            $list = [];

            if($res){
                $list = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $list;
        }
    }
