<?php
    namespace App\Models;
  
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    

    class CategoryModel extends Model {
        
        protected function getFields(): array{
            return [
                'category_id' => new Field((new NumberValidator())->setIntegerLength(11), false),

                'name'        => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'is_visible'  => new Field(new BitValidator() ),
                
            ];
        }

        public function getByName(string $name){
            return $this->getByFieldName('name', $name);
        }
        
        public function getAllBySearch(string $keywords) {
            $sql = 'SELECT * FROM `category` WHERE `name` LIKE ?';

            $keywords = '%' .$keywords . '%';

            $prep = $this->getConnection()->prepare($sql);
            if(!$prep){
                return [];
            }
            $res = $prep->execute([$keywords]);
            if(!$res){
                return[];
            }
            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
    }