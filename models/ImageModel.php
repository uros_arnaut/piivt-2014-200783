<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;

    class ImageModel extends Model {
        protected function getFields(): array {
            return [
                'image_id'      => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at'    => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),
                
                'vehicle_id'    => new Field((new NumberValidator())->setIntegerLength(11), true),
                'title'         => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
                'path'          => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
            ];
        }

        public function getAllByImageId(int $imageid): array {
            return $this->getAllByFieldName('image_id', $imageId);
        }
    }
