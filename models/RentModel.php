<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class RentModel extends Model {
        protected function getFields(): array {
            return [
                'rent_id'           => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                
                'delivery_place'    => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
                'return_place'      => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
                'delivered_at'      => new Field((new DateTimeValidator())->allowDate()->allowTime() ),
                'returned_due_at'   => new Field((new DateTimeValidator())->allowDate()->allowTime() ),
                'returned_at'       => new Field((new DateTimeValidator())->allowDate()->allowTime() ),
                
                'user_id'           => new Field((new NumberValidator())->setIntegerLength(11), true),
                'vehicle_id'        => new Field((new NumberValidator())->setIntegerLength(11), true),
                'milage'            => new Field((new NumberValidator())->setIntegerLength(11), true),
                'spent_gas'         => new Field((new NumberValidator())->setIntegerLength(11), true),
            ];
        }

        public function getAllByRentId(int $rentId): array {
            return $this->getAllByFieldName('rent_id', $rentId);
        }

        public function getAllByUserId(int $userId): array {
            return $this->getAllByFieldName('user_id', $userId);
        }
    }
