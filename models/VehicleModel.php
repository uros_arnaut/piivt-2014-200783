<?php
    namespace App\Models;
    
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class VehicleModel extends Model{
        protected function getFields(): array{
            return [
                'vehicle_id'     => new Field((new NumberValidator())->setIntegerLength(11), false),

                'initial_milage' => new Field((new NumberValidator())->setIntegerLength(11) ),
                'admin_id'       => new Field((new NumberValidator())->setIntegerLength(11) ),
                'category_id'    => new Field((new NumberValidator())->setIntegerLength(11) ),

                'image_title'    => new Field((new StringValidator)->setMaxLength(255) ),
                'path'           => new Field((new StringValidator)->setMaxLength(255) ),
            ];
        }

        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
        }
        
        public function getAllByName(string $name) {
            return $this->getByFieldName('name', $name);
        }

        public function getAllByVehicleId(int $vehicleId): array {
            return $this->getAllByFieldName('vehicle_id', $vehicleId);
        }

        public function getFeatures(int $vehicleId): array {
            $sql = 'SELECT `feature`.`name`, `vehicle_feature`.`value` FROM `feature` 
            INNER JOIN `vehicle_feature` ON `feature`.`feature_id` = `vehicle_feature`.`feature_id` 
            INNER JOIN `vehicle` ON `vehicle`.`vehicle_id` = `vehicle_feature`.`vehicle_id` 
            WHERE `vehicle`.`vehicle_id` = ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$vehicleId]);
            $list = [];

            if($res){
                $list = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $list;
        }

        public function getFeatureValues(int $categoryId): array {
            $sql = 'SELECT `feature`.`name`, `vehicle_feature`.`value` FROM `feature` 
            INNER JOIN `vehicle_feature` ON `feature`.`feature_id` = `vehicle_feature`.`feature_id` 
            INNER JOIN `vehicle` ON `vehicle`.`vehicle_id` = `vehicle_feature`.`vehicle_id` 
            INNER JOIN `category` ON `category`.`category_id` = `vehicle`.`category_id` 
            WHERE `category`.`category_id` = ?';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$categoryId]);
            
            $list = [];

            if($res){
                $list = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $list;
        }

        public function getShortData(): array {
            $sql = 'SELECT * FROM `view_vehicle_short_data`;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute();
            $list = [];

            if($res){
                $list = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $list;
        }

    }