<?php
    namespace App\Models;
    
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;
    

    class VehicleViewModel extends Model{

        protected function getFields(): array{
            return [
                'vehicle_view_id' => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'      => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),
                
                'vehicle_id'      => new Field((new NumberValidator())->setIntegerLength(11) ),
                'ip_address'      => new Field((new \App\Validators\StringValidator(7, 255)) ),
                'user_agent'      => new Field((new \App\Validators\StringValidator(0, 255)) )
            ];
        }

        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
        }
        
        public function getAllByName(string $name) {
            return $this->getByFieldName('name', $name);
        }
    }