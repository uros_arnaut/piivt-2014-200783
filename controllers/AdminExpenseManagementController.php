<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class AdminExpenseManagementController extends \App\Core\Role\AdminRoleController {
        
        public function expenses() {
            $adminId = $this->getSession()->get('admin_id');

            $expenseModel = new \App\Models\ExpenseModel($this->getDatabaseConnection());
            $expenses = $expenseModel->getAll();

            $this->set('expenses', $expenses);
        }
        
        public function getAdd(){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postAdd(){

            $intName = filter_input(INPUT_POST, 'intervention_name', FILTER_SANITIZE_STRING);
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_INT);
            $vehicle = filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT);

            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(255);
            if(!$stringValidator->isValid($intName)){
               $this->set('message', 'Doslo je do greske: Ime intervencije nije ispravnog formata!');
                return; 
            }

            $addData = [
                'intervention_name'  => $intName,
                'price'              => $price,
                'vehicle_id'         => $vehicle,
            ];

            $expenseModel = new \App\Models\ExpenseModel($this->getDatabaseConnection());
            $expenseId = $expenseModel->add($addData);

            if(!$expenseId){
                $this->set('message', 'Nije uspesno dodavanje.');
                return;
            }

            $this->redirect( \Configuration::BASE . 'admin/expenses');
        }

        public function getEdit($expenseId){
            $expenseModel = new \App\Models\ExpenseModel($this->getDatabaseConnection());
            $expense = $expenseModel->getById($expenseId);

            if(!$expense){
                $this->redirect( \Configuration::BASE . 'admin/expenses');
                return;
            }

            $this->set('expense', $expense);

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postEdit($expenseId) {
            $this->getEdit($expenseId);

            $intName = filter_input(INPUT_POST, 'intervention_name', FILTER_SANITIZE_STRING);
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_INT);
            $vehicle = filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT);

            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(255);
            if(!$stringValidator->isValid($intName)){
               $this->set('message', 'Doslo je do greske: Ime intervencije nije ispravnog formata!');
                return; 
            }

            $editData = [
                'intervention_name'  => $intName,
                'price'              => $price,
                'vehicle_id'         => $vehicle,
            ];
            
            $expenseModel = new \App\Models\ExpenseModel($this->getDatabaseConnection());
            $res = $expenseModel ->editById($expenseId, $editData);

            if(!$res){
                $this->set('message', 'Nije bilo moguce izmeniti.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/expenses');
        }
    }