<?php
    namespace App\Controllers;

    class CategoryController extends \App\Core\Controller{

        public function show($id){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            if(!$category){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('category', $category);
            
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehiclesInCategory = $vehicleModel->getAllByCategoryId($id);
            $this->set('vehiclesInCategory', $vehiclesInCategory);

            $vehicleShortData = $vehicleModel->getShortData();
            $this->set('vehicleShortData', $vehicleShortData);


            #features
            $featureInCategories = $vehicleModel->getFeatureValues($id);
            $this->set('featureInCategories', $featureInCategories);

            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $price = $priceModel->getById($id);

            $this->set('price', $price);


        }
        

        public function categories() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $this->set('categories', $categoryModel->getAll());
        }

        private function normaliseKeywords(string $keywords): string {
            $keywords = $this->normaliseKeywords($q);
            return $keywords;
        }

        public function postSearch() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

            $keywords = trim($q);
            $keywords = \preg_replace('/ +/', ' ', $keywords);

            $categoris = $categoryModel->getAllBySearch($q);

            $this->set('categories', $categoris);
        }
    }
