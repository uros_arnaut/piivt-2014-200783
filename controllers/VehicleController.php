<?php
    namespace App\Controllers;

    class VehicleController extends \App\Core\Controller{

        public function show($id){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicle = $vehicleModel->getById($id);

            if(!$vehicle){
                $this->redirect(\Configuration::BASE);
                exit;
            }

            $this->set('vehicle', $vehicle);
            
            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $price = $priceModel->getById($id);

            $this->set('price', $price);
            
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $this->set('categories', $categoryModel->getAll());

            $vehicleViewModel = new \App\Models\VehicleViewModel($this->getDatabaseConnection());

            $ipAddres = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');            

            $vvId = $vehicleViewModel->add(
                [
                    'vehicle_id'  => $id,
                    'ip_address'  => $ipAddres,
                    'user_agent'  => $userAgent 
                ]
            );

            #features
            $featuresInVehicle = $vehicleModel->getFeatures($id);
            $this->set('featuresInVehicle', $featuresInVehicle);

            #registration
            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registration = $registrationModel->getById($id);
            $this->set('registration', $registration);
        }

        public function vehicles() {
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);

            $vehicleShortData = $vehicleModel->getShortData();
            $this->set('vehicleShortData', $vehicleShortData);
        }
    }