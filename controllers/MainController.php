<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class MainController extends \App\Core\Controller{

        public function home(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

            #$staraVrednost = $this->getSession()->get('brojac', 0);
            #$novaVrednost = $staraVrednost + 1;
            #$this->getSession()->put('brojac', $novaVrednost);
            #$this->set('podatak', $novaVrednost);
        }

        #dodat novi kod
        public function getRegister() {
            
        }

        public function postRegister() {
            $name       = \filter_input(INPUT_POST, 'reg_name', FILTER_SANITIZE_STRING);
            $surname    = \filter_input(INPUT_POST, 'reg_surname', FILTER_SANITIZE_STRING);
            $id_number  = \filter_input(INPUT_POST, 'reg_id_number', FILTER_SANITIZE_NUMBER_INT);
            $email      = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
            $username   = \filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
            $password1  = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
            $password2  = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

            if ($password1 !== $password2) {
                $this->set('message', 'Doslo je do greške: Niste uneli dva puta istu lozinku.');
                return;
            }

            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(7)
                ->setMaxLength(120)
                ->isValid($password1);

            if ( !$validanPassword) {
                $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
                return;
            }

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

            $user = $userModel->getByFieldName('email', $email);
            if ($user) {
                $this->set('message', 'Doslo je do greške: Već postoji korisnik sa tom adresom e-pošte.');
                return;
            }

            $user = $userModel->getByFieldName('username', $username);
            if ($user) {
                $this->set('message', 'Doslo je do greške: Već postoji korisnik sa tim korisničkim imenom.');
                return;
            }


            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime nije ispravnog formata!');
                return; 
            }


            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($surname)){
               $this->set('message', 'Doslo je do greske: Prezime nije ispravnog formata!');
                return; 
            }


            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($email)){
               $this->set('message', 'Doslo je do greske: E-mail nije ispravnog formata!');
                return; 
            }


            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

            $userId = $userModel->add([
                'name'          => $name,
                'surname'       => $surname,
                'id_number'     => $id_number,
                'email'         => $email,
                'username'      => $username,
                'password_hash' => $passwordHash,     
            ]);

            if (!$userId) {
                $this->set('message', 'Doslo je do greške: Nije bilo uspešno registrovanje naloga.');
                return;
            }

            $this->set('message', 'Napravljen je novi nalog. Sada možete da se prijavite.');
        }

    #USER LOGIN
    public function getLogin() {
        
    }

    public function postLogin() {
        $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
        $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);


        $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($username)){
               $this->set('message', 'Doslo je do greske: Korisnicko ime nije ispravnog formata!');
                return; 
            }


        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password);

        if ( !$validanPassword) {
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
            return;
        }

        $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

        $user = $userModel->getByFieldName('username', $username);

        if (!$user) {
            $this->set('message', 'Doslo je do greške: Ne postoji korisnik sa tim korisničkim imenom.');
            return;
        }

        if (!password_verify($password, $user->password_hash)) {
            sleep(1); #usporava brute force attack, tako sto ceka 1sec na msg za neispravnu pswd
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravna.');
            return;
        }

        $this->getSession()->put('user_id', $user->user_id);
        $this->getSession()->save();

        $userLoginModel = new \App\Models\UserLoginModel($this->getDatabaseConnection());
            
            $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            
            $log = $userLoginModel->add(
                [
                    'user_id' => $this->getSession()->get('user_id'),
                    'ip_address' => $ipAddress
                ]
            );

        $this->redirect(\Configuration::BASE . 'user/profile');
    }
    #USER LOGIN.
    
    public function getLogout() {
        $this->getSession()->remove('user_id');
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE);
    }
    
	#ADMIN REGISTER
	public function getAdminRegister() {
			
	}

    public function postRegisterAdmin() {
        $name       = \filter_input(INPUT_POST, 'reg_name', FILTER_SANITIZE_STRING);
        $surname    = \filter_input(INPUT_POST, 'reg_surname', FILTER_SANITIZE_STRING);
        $email      = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
        $username   = \filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
        $password1  = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
        $password2  = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

        if ($password1 !== $password2) {
            $this->set('message', 'Doslo je do greške: Niste uneli dva puta istu lozinku.');
            return;
        }

        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password1);
        
        if ( !$validanPassword) {
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
            return;
        }
        
        $adminModel = new \App\Models\AdminModel($this->getDatabaseConnection());

        $admin = $adminModel->getByFieldName('email', $email);
        if ($admin) {
            $this->set('message', 'Doslo je do greške: Već postoji korisnik sa tom adresom e-pošte.');
            return;
        }

        $admin = $adminModel->getByFieldName('username', $username);
        if ($admin) {
            $this->set('message', 'Doslo je do greške: Već postoji korisnik sa tim korisničkim imenom.');
            return;
        }

        $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime nije ispravnog formata!');
                return; 
            }

        $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
        if(!$stringValidator->isValid($surname)){
            $this->set('message', 'Doslo je do greske: Korisnicko ime nije ispravnog formata!');
            return; 
        }

        $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

        $adminId = $adminModel->add([
            'name'          => $name,
            'surname'       => $surname,
            'email'         => $email,
            'username'      => $username,
            'password_hash' => $passwordHash,     
        ]);

        if (!$adminId) {
            $this->set('message', 'Doslo je do greške: Nije bilo uspešno registrovanje naloga.');
            return;
        }

        $this->set('message', 'Napravljen je novi nalog. Sada možete da se prijavite.');
    }
	#ADMIN REGISTER.
	
	#ADMIN LOGIN
	public function getLoginAdmin() {

        }

    public function postLoginAdmin() {
        $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
        $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password);

        if ( !$validanPassword) {
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
            return;
        }

        $adminModel = new \App\Models\AdminModel($this->getDatabaseConnection());

        $admin = $adminModel->getByFieldName('username', $username);
        if (!$admin) {
            $this->set('message', 'Doslo je do greške: Ne postoji ADMIN sa tim korisničkim imenom.');
            return;
        }

        if (!password_verify($password, $admin->password_hash)) {
            sleep(1); #usporava brute force attack, tako sto ceka 1sec na msg za neispravnu pswd
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravna.');
            return;
        }

        $this->getSession()->put('admin_id', $admin->admin_id);
        $this->getSession()->save();

        $adminLoginModel = new \App\Models\AdminLoginModel($this->getDatabaseConnection());
            
            $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            
            $log = $adminLoginModel->add(
                [
                    'admin_id' => $this->getSession()->get('admin_id'),
                    'ip_address' => $ipAddress
                ]
            );

        $this->redirect(\Configuration::BASE . 'admin/profile');
    }
    #ADMIN LOGIN.

        public function getLogoutAdmin() {
            $this->getSession()->remove('admin_id');
            $this->getSession()->save();

            $this->redirect(\Configuration::BASE);
        }

        public function getContact(){
            
        }

        public function postContact(){
            $name    = \filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $surname = \filter_input(INPUT_POST, 'surrname', FILTER_SANITIZE_STRING);
            $email   = \filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
            $phone   = \filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_NUMBER_INT);
            $message = \filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime nije ispravnog formata!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($surname)){
               $this->set('message', 'Doslo je do greske: Prezime nije ispravnog formata!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(255);
            if(!$stringValidator->isValid($email)){
               $this->set('message', 'Doslo je do greske: E-mail nije ispravnog formata!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(20)->setMaxLength(255);
            if(!$stringValidator->isValid($message)){
               $this->set('message', 'Doslo je do greske: Poruka je prekratka! Minimalno 20 karaktera.');
                return; 
            }
            
            $html = '<!doctype html><html><meta charset="utf-8"</head></body>';
            $html .= 'Poruka od: ' . \htmlspecialchars($name . ' ' . $surname) . '<br>';
            $html .= 'Mejl za kontakta: ' . \htmlspecialchars($email);
            $html .= 'Telefon za kontakta: ' . \htmlspecialchars($phone);
            $html .= 'Poruka korisnika: ' . \htmlspecialchars($message);
            $html .= '</body></html>';

            $mailer = new \PHPMailer\PHPMailer\PHPMailer();
            $mailer->isSMTP();
            $mailer->Host = \Configuration::MAIL_HOST;
            $mailer->Port = \Configuration::MAIL_PORT;
            $mailer->SMTPSecure = \Configuration::MAIL_PROTOCOL;
            $mailer->SMTPAuth = true;
            $mailer->Username = \Configuration::MAIL_USERNAME;
            $mailer->Password = \Configuration::MAIL_PASSWORD;

            $mailer->isHTML(true);

            $mailer->Body = $html;
            $mailer->Subject = 'Nova poruka';
            $mailer->setFrom(\Configuration::MAIL_USERNAME);

            $mailer->addAddress(\Configuration::ADMIN_MAIL);
            
            $ok = $mailer->send();

            if ($ok) {
                $this->set('message', 'Poruka je poslata. Hvala.');
                return;
            }

            $this->set('message', 'Greska. Probajte ponovo za par minuta.');
        }
    }