<?php
    namespace App\Controllers;

    class UserDashboardController extends \App\Core\Role\UserRoleController {
        public function index() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

            $userId = $this->getSession()->get('user_id');
            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $rents = $rentModel->getAllByUserId($userId);
            $this->set('rents', $rents);
        }
    }
