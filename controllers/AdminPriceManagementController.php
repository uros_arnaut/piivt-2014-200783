<?php
    namespace App\Controllers;

    class AdminPriceManagementController extends \App\Core\Role\AdminRoleController {
        
        public function prices() {
            $adminId = $this->getSession()->get('admin_id');

            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $prices = $priceModel->getAll();

            $this->set('prices', $prices);
        }
        
        public function getAdd(){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postAdd(){

            $addData = [
                'price'          => \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_INT),
                'vehicle_id'      => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT)
            ];

            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $priceId = $priceModel->add($addData);

            if(!$priceId){
                $this->set('message', 'Nije uspesno dodavanje cene');
                return;
            }

            $this->redirect( \Configuration::BASE . 'admin/prices');
        }

        public function getEdit($priceId){
            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $price = $priceModel->getById($priceId);

            if(!$price){
                $this->redirect( \Configuration::BASE . 'admin/prices');
                return;
            }

            $this->set('price', $price);

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postEdit($priceId) {
            $this->getEdit($priceId);

            $editData = [
                'price'          => \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_INT),
                'vehicle_id'      => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT)
            ];
            
            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $res = $priceModel ->editById($priceId, $editData);

            if(!$res){
                $this->set('message', 'Nije bilo moguce izmeniti cenu.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/prices');
        }
    }