<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class AdminRegistrationManagementController extends \App\Core\Role\AdminRoleController {
        
        public function registrations() {
            $adminId = $this->getSession()->get('admin_id');

            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registrations = $registrationModel->getAll();

            $this->set('registrations', $registrations);
        }
        
        public function getAdd(){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);

            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registrations = $registrationModel->getAllByFeatureValue();
            $this->set('registrations', $registrations);
        }

        public function postAdd(){
        
            $regAt = filter_input(INPUT_POST, 'registred_at', FILTER_SANITIZE_STRING);
            $expiresAt  = filter_input(INPUT_POST, 'expires_at', FILTER_SANITIZE_STRING);
            $reg = filter_input(INPUT_POST, 'registration', FILTER_SANITIZE_STRING);
            $vehicle = filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT);

            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(9);
            if(!$stringValidator->isValid($reg)){
               $this->set('message', 'Doslo je do greske: Registracija nije ispravnog formata!');
                return; 
            }

            $addData = [
                'registred_at'  => $regAt,
                'expires_at'    =>  $expiresAt,
                'registration'  => $reg,
                'vehicle_id'    => $vehicle
            ];

            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registrationId = $registrationModel->add($addData);

            if(!$registrationId){
                $this->set('message', 'Nije uspesno dodavanje registracije');
                return;
            }

            $this->redirect( \Configuration::BASE . 'admin/registrations');
        }

        public function getEdit($registrationId){
            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registration = $registrationModel->getAllByRegistrationId($registrationId);

            if(!$registration){
                $this->redirect( \Configuration::BASE . 'admin/registrations');
                return;
            }

            $reg = $registrationModel->getById($registrationId);

            if(!$reg){
                $this->redirect( \Configuration::BASE . 'admin/registrations');
                return;
            }

            $this->set('registration', $reg);
        
            $reg->registred_at = str_replace(' ', 'T', substr($reg->registred_at, 0, 16));
            $reg->expires_at = str_replace(' ', 'T', substr($reg->expires_at, 0, 16));
            $this->set('reg', $reg);

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);

            $registrations = $registrationModel->getAllByFeatureValue();
            $this->set('registrations', $registrations);
        }

        public function postEdit($registrationId) {
            $this->getEdit($registrationId);

            $reg = filter_input(INPUT_POST, 'registration', FILTER_SANITIZE_STRING);
            
            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(9);
            if(!$stringValidator->isValid($reg)){
               $this->set('message', 'Doslo je do greske: Registracija nije ispravnog formata!');
                return; 
            }

            $editData = [
                'registred_at'  => \filter_input(INPUT_POST, 'registred_at', FILTER_SANITIZE_STRING),
                'expires_at'    => \filter_input(INPUT_POST, 'expires_at', FILTER_SANITIZE_STRING),
                'registration'  => $reg,
                'vehicle_id'    => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT)
            ];
            
            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $res = $registrationModel ->editById($registrationId, $editData);

            if(!$res){
                $this->set('message', 'Nije bilo moguce izmeniti registraciju.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/registrations');
        }
    }