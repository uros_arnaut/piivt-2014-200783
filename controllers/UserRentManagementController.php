<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class UserRentManagementController extends \App\Core\Role\UserRoleController {
        
        public function rents() {
            $userId = $this->getSession()->get('user_id');

            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $rents = $rentModel->getAllByUserId($userId);

            $this->set('rents', $rents);
        }
        
        public function getAdd(){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postAdd(){
            
            $deliveryP = filter_input(INPUT_POST, 'delivery_place', FILTER_SANITIZE_STRING);
            $returnP = filter_input(INPUT_POST, 'return_place', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($deliveryP)){
               $this->set('message', 'Doslo je do greske: Ime mesta isporuke nije ispravnog formata!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($returnP)){
               $this->set('message', 'Doslo je do greske: Ime mesta vracanja nije ispravnog formata!');
                return; 
            }

            $addData = [
                'delivery_place'  => $deliveryP,
                'return_place'    => $returnP,
                'delivered_at'    => \filter_input(INPUT_POST, 'delivered_at', FILTER_SANITIZE_STRING),
                'returned_at'     => \filter_input(INPUT_POST, 'returned_at', FILTER_SANITIZE_STRING),
                'user_id'         => $this->getSession()->get('user_id'),
                'vehicle_id'      => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT)
            ];

            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $rentId = $rentModel->add($addData);

            if(!$rentId){
                $this->set('message', 'Nije uspesna rezervacija');
                return;
            }

            $this->redirect( \Configuration::BASE . 'user/rents');
        }

        public function getEdit($rentId){
            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $rent = $rentModel->getById($rentId);

            if(!$rent){
                $this->redirect( \Configuration::BASE . 'user/rents');
                return;
            }

            /*if($rent->user_id != $this->getSession()->get('user_id')){
                $this->redirect( \Configuration::BASE . 'user/rents');
                return;
            }*/

            $rent->delivered_at = str_replace(' ', 'T', substr($rent->delivered_at, 0, 16));
            $rent->returned_at = str_replace(' ', 'T', substr($rent->returned_at, 0, 16));

            $this->set('rent', $rent);
            #return $rentModel;

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postEdit($rentId) {
            $this->getEdit($rentId);

            $deliveryP = filter_input(INPUT_POST, 'delivery_place', FILTER_SANITIZE_STRING);
            $returnP = filter_input(INPUT_POST, 'return_place', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($deliveryP)){
               $this->set('message', 'Doslo je do greske: Ime mesta isporuke nije ispravnog formata!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($returnP)){
               $this->set('message', 'Doslo je do greske: Ime mesta vracanja nije ispravnog formata!');
                return; 
            }

            $editData = [
                'delivery_place'  => $deliveryP,
                'return_place'    => $returnP,
                'delivered_at'    => \filter_input(INPUT_POST, 'delivered_at', FILTER_SANITIZE_STRING),
                'returned_at'     => \filter_input(INPUT_POST, 'returned_at', FILTER_SANITIZE_STRING),
                'vehicle_id'      => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT)
            ];
            
            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $res = $rentModel ->editById($rentId, $editData);

            if(!$res){
                $this->set('message', 'Nije bilo moguce izmeniti rezervaciju.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'user/rents');
        }
    }