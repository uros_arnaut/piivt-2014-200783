<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class AdminFeatureManagementController extends \App\Core\Role\AdminRoleController {
        public function features() {
            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $features = $featureModel->getAll();
            $this->set('features', $features);
        }

        public function getEdit($featureId) {
            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $feature = $featureModel->getById($featureId);

            if (!$feature) {
                $this->redirect(\Configuration::BASE . 'admin/features');
            }

            $this->set('feature', $feature);

            return $featureModel;
        }

        public function postEdit($featureId) {
            $featureModel = $this->getEdit($featureId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(255);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime karakteristike nije ispravnog formata!');
                return; 
            }

            $featureModel->editById($featureId, [
                'name' => $name
            ]);

            $this->redirect(\Configuration::BASE . 'admin/features');
        }

        public function getAdd() {

        }

        public function postAdd() {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(255);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime karakteristike nije ispravnog formata!');
                return; 
            }

            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());

            $featureId = $featureModel->add([
                'name' => $name
            ]);

            if ($featureId) {
                $this->redirect(\Configuration::BASE . 'admin/features');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovu karatkeristiku!');
        }
    }
