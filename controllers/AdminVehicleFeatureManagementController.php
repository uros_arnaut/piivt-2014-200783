<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class AdminVehicleFeatureManagementController extends \App\Core\Role\AdminRoleController {
        
        public function vehicleFeatures() {
            $adminId = $this->getSession()->get('admin_id');

            $vehicleFeatureModel = new \App\Models\VehicleFeatureModel($this->getDatabaseConnection());
            $vehicleFeatures = $vehicleFeatureModel->getAll();

            $this->set('vehicleFeatures', $vehicleFeatures);
        }
        
        public function getAdd(){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);

            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $features = $featureModel->getAll();
            $this->set('features', $features);
        }

        public function postAdd(){
            
            $value = filter_input(INPUT_POST, 'value', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(1)->setMaxLength(255);
            if(!$stringValidator->isValid($value)){
               $this->set('message', 'Doslo je do greske: Registracija nije ispravnog formata!');
                return; 
            }

            $addData = [
                'feature_id'    => \filter_input(INPUT_POST, 'feature_id', FILTER_SANITIZE_NUMBER_INT),
                'vehicle_id'    => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT),
                'value'         => $value
            ];

            $vehicleFeatureModel = new \App\Models\VehicleFeatureModel($this->getDatabaseConnection());
            $vehicleFeatureId = $vehicleFeatureModel->add($addData);

            if(!$vehicleFeatureId){
                $this->set('message', 'Nije uspesno dodavanje vrednosti karakteristike');
                return;
            }

            $this->redirect( \Configuration::BASE . 'admin/vehicleFeatures');
        }

        public function getEdit($vehicleFeatureId){
            $vehicleFeatureModel = new \App\Models\VehicleFeatureModel($this->getDatabaseConnection());
            $vehicleFeatures = $vehicleFeatureModel->getAllByVehicleFeatureId($vehicleFeatureId);

            if(!$vehicleFeatures){
                $this->redirect( \Configuration::BASE . 'admin/vehicleFeatures');
                return;
            }

            $this->set('vehicleFeatures', $vehicleFeatures);

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);

            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $features = $featureModel->getAll();
            $this->set('features', $features);

            $vehicleFeature = $vehicleFeatureModel->getById($vehicleFeatureId);
            $this->set('vehicleFeature', $vehicleFeature);
        }

        public function postEdit($vehicleFeatureId) {
            $this->getEdit($vehicleFeatureId);

            $value = filter_input(INPUT_POST, 'value', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(1)->setMaxLength(255);
            if(!$stringValidator->isValid($value)){
               $this->set('message', 'Doslo je do greske: Registracija nije ispravnog formata!');
                return; 
            }

            $editData = [
                'feature_id'    => \filter_input(INPUT_POST, 'feature_id', FILTER_SANITIZE_NUMBER_INT),
                'vehicle_id'    => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT),
                'value'         => $value,
            ];
            
            $vehicleFeatureModel = new \App\Models\VehicleFeatureModel($this->getDatabaseConnection());
            $res = $vehicleFeatureModel ->editById($vehicleFeatureId, $editData);

            if(!$res){
                $this->set('message', 'Nije bilo moguce izmeniti vrednost karatkteristike.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/vehicleFeatures');
        }
    }