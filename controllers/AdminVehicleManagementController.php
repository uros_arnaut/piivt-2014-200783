<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class AdminVehicleManagementController extends \App\Core\Role\AdminRoleController {
        
        public function vehicles() {
            $adminId = $this->getSession()->get('admin_id');

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();

            $this->set('vehicles', $vehicles);

            $vehicleShortData = $vehicleModel->getShortData();
            $this->set('vehicleShortData', $vehicleShortData);

        }
        
        public function getAdd(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function postAdd(){
        
            $addData = [
                'initial_milage'  => \filter_input(INPUT_POST, 'initial_milage', FILTER_SANITIZE_NUMBER_INT),
                'admin_id'        => $this->getSession()->get('admin_id'),
                'category_id'     => \filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT),
                # 'image_title'     => \filter_input(INPUT_POST, 'image_title', FILTER_SANITIZE_STRING)
            ];

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicleId = $vehicleModel->add($addData);

            if(!$vehicleId){
                $this->set('message', 'Nije uspesno dodavanje vozila');
                return;
            }

            $uploadPath = $this->doImageUpload('image', $vehicleId);
            if (!$uploadPath){
                $this->set('message', 'Vozilo je izmenjeno, ali nije dodata nova slika.');
                return;
            }

            $imgTitle = filter_input(INPUT_POST, 'image_title', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($imgTitle)){
               $this->set('message', 'Doslo je do greske: Naslov slike nije ispravnog formata!');
                return; 
            }

            $addDataZaImage = [
                'title' => $imgTitle,
                'vehicle_id' => $vehicleId,
                'path' => $uploadPath
            ];

            $imageModel = new \App\Models\ImageModel($this->getDatabaseConnection());
            $imageModel->add($addDataZaImage);

            $this->redirect( \Configuration::BASE . 'admin/vehicles');
        }

        public function getEdit($vehicleId){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAllByVehicleId($vehicleId);

            if(!$vehicles){
                $this->redirect( \Configuration::BASE . 'admin/vehicles');
                return;
            }

            $this->set('vehicles', $vehicles);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function postEdit($vehicleId) {
            $this->getEdit($vehicleId);

            $imgTitle = filter_input(INPUT_POST, 'image_title', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($imgTitle)){
               $this->set('message', 'Doslo je do greske: Naslov slike nije ispravnog formata!');
                return; 
            }

            $editData = [
                'initial_milage'  => \filter_input(INPUT_POST, 'initial_milage', FILTER_SANITIZE_NUMBER_INT),
                'category_id'     => \filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT),
                'image_title'     => $imgTitle
            ];
            
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $res = $vehicleModel ->editById($vehicleId, $editData);

            if(!$res){
                $this->set('message', 'Nije bilo moguce izmeniti vozilo.');
                return;
            }

            $uploadPath = $this->doImageUpload('image', $vehicleId);
            if (!$uploadPath){
                $this->set('message', 'Vozilo je izmenjeno, ali nije dodata nova slika.');
                return;
            }

            #edit za img
            $editDataZaImage = [
                'title' => \filter_input(INPUT_POST, 'image_title', FILTER_SANITIZE_STRING),
                'vehicle_id' => $vehicleId,
                'path' => $uploadPath
            ];

            $imageModel = new \App\Models\ImageModel($this->getDatabaseConnection());
            $imageModel->add($editDataZaImage);

            $this->redirect(\Configuration::BASE . 'admin/vehicles');
        }

        private function doImageUpload(string $fieldName, string $fileName): string {
            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $uploadPath);
            
            $file->setName($fileName);
            $file->addValidations([
                new \Upload\Validation\Mimetype([
                    'image/jpeg',
                    'image/png'
                    ]),
                new \Upload\Validation\Size("3M"),
                #new \Upload\Validation\Dimentions(320, 240)
            ]);
            
            try {
                $file->upload();
                return $file->getNameWithExtension();
            } catch (Exception $e){
                $this->set('message', 'Greska: ' .$e->getMessage());
                return '';
            }
        }

        #REGISTRACIJA unutar vehicle cms-a

        public function registrations($vehicleId) {
            $adminId = $this->getSession()->get('admin_id');
            
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getById($vehicleId);
            $this->set('vehicles', $vehicles);

            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registrations = $registrationModel->getAllByFieldName('vehicle_id', $vehicleId);
            
           

            $this->set('registrations', $registrations);
        }

        public function getAddReg($vehicleId, $registrationId) {

        }

        public function postAddReg($vehicleId, $registrationId) {

            $regVh = filter_input(INPUT_POST, 'registration', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(9);
            if(!$stringValidator->isValid($regVh)){
               $this->set('message', 'Doslo je do greske: Registracija nije ispravnog formata!');
                return; 
            }

            $addData = [
                'registred_at'  => \filter_input(INPUT_POST, 'registred_at', FILTER_SANITIZE_STRING),
                'expires_at'    => \filter_input(INPUT_POST, 'expires_at', FILTER_SANITIZE_STRING),
                'registration'  => $regVh
            ];

            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registration = $registrationModel->add($addData);

            if (!$registration){
                $this->set('message', 'Nije bilo moguce dodati registraciju.');
                return;
            }

            if ($registration->vehicle_id !== $vehicleId){
                $this->set('message', 'Nije bilo moguce dodati registraciju.');
                return;
            }

            $registration->registred_at = str_replace(' ', 'T', substr($registration->registred_at, 0, 16));
            $registration->expires_at = str_replace(' ', 'T', substr($registration->expires_at, 0, 16));
            $this->set('registration', $registration);
        }
        #########

        public function getEditReg($vehicleId, $registrationId) {
            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registration = $registrationModel->getById($registrationId);

            if (!$registration){
                $this->redirect( \Configuration::BASE . 'admin/vehicles');
                return;
            }

            if ($registration->vehicle_id !== $vehicleId){
                $this->redirect( \Configuration::BASE . 'admin/vehicles');
                return;
            }

            $registration->registred_at = str_replace(' ', 'T', substr($registration->registred_at, 0, 16));
            $registration->expires_at = str_replace(' ', 'T', substr($registration->expires_at, 0, 16));
            $this->set('registration', $registration);
        }

        public function postEditReg($vehicleId, $registrationId) {
            $this->getEditReg($vehicleId, $registrationId); # Samo radi svih provera gore obavljenih...

            $registredAt  = \filter_input(INPUT_POST, 'registred_at', FILTER_SANITIZE_STRING);
            $expiresAt    = \filter_input(INPUT_POST, 'expires_at', FILTER_SANITIZE_STRING);
            $registration = \filter_input(INPUT_POST, 'registration', FILTER_SANITIZE_STRING);

            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            
            $status = $registrationModel->editById($registrationId, [
                'registred_at' => $registredAt,
                'expires_at'   => $expiresAt,
                'registration' => $registration
            ]);

            if (!$status) {
                $this->set('message', 'Error!');
                return;
            }

            $this->redirect( \Configuration::BASE . 'admin/vehicle/' . $vehicleId . '/registrations');
        }

        public function show($id){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicle = $vehicleModel->getById($id);

            if(!$vehicle){
                $this->redirect(\Configuration::BASE);
                exit;
            }

            $this->set('vehicle', $vehicle);
            
            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $price = $priceModel->getById($id);

            $this->set('price', $price);
            
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $this->set('categories', $categoryModel->getAll());

            #features
            $featuresInVehicle = $vehicleModel->getFeatures($id);
            $this->set('featuresInVehicle', $featuresInVehicle);

            #registration
            $registrationModel = new \App\Models\RegistrationModel($this->getDatabaseConnection());
            $registration = $registrationModel->getById($id);
            $this->set('registration', $registration);
        }
    }
