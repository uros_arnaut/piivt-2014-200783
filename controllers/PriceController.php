<?php
    namespace App\Controllers;

    class PriceController extends App\Core\Controller{

        public function show($id){
            $priceModel = new \App\Models\PriceModel($this->getDatabaseConnection());
            $price = $priceModel->getById($id);

            $this->set('price', $price);
            
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $prices = $vehicleModel->getAllByVehicleId($id);
        }
    }