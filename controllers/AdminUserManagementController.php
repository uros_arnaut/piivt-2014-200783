<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class AdminUserManagementController extends \App\Core\Role\AdminRoleController {
        
        public function users() {
            $adminId = $this->getSession()->get('admin_id');

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $users = $userModel->getAll();

            $this->set('users', $users);
        }

        private function normaliseKeywords(string $keywords): string {
            $keywords = $this->normaliseKeywords($q);
            return $keywords;
        }

        public function postSearch() {
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

            $keywords = trim($q);
            $keywords = \preg_replace('/ +/', ' ', $keywords);

            $users = $userModel->getAllBySearch($q);

            $this->set('users', $users);
        }
    }