<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;

    class AdminRentManagementController extends \App\Core\Role\AdminRoleController {
        
        public function rents() {
            $adminId = $this->getSession()->get('admin_id');

            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $rents = $rentModel->getAll();

            $this->set('rents', $rents);
        }
        
        public function getAdd(){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postAdd(){

            $deliveryP = filter_input(INPUT_POST, 'delivery_place', FILTER_SANITIZE_STRING);
            $returnP = filter_input(INPUT_POST, 'return_place', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($deliveryP)){
               $this->set('message', 'Doslo je do greske: Ime mesta isporuke nije ispravnog formata!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(2)->setMaxLength(255);
            if(!$stringValidator->isValid($returnP)){
               $this->set('message', 'Doslo je do greske: Ime mesta vracanja nije ispravnog formata!');
                return; 
            }

            $addData = [
                'delivery_place'  => $deliveryP,
                'return_place'    => $returnP,
                'delivered_at'    => \filter_input(INPUT_POST, 'delivered_at', FILTER_SANITIZE_STRING),
                'returned_due_at' => \filter_input(INPUT_POST, 'returned_due_at', FILTER_SANITIZE_STRING),
                'returned_at'     => \filter_input(INPUT_POST, 'returned_at', FILTER_SANITIZE_STRING),
                #'user_id'         => \filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_NUMBER_INT),
                'vehicle_id'      => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT),
                'milage'          => \filter_input(INPUT_POST, 'milage', FILTER_SANITIZE_NUMBER_INT),
                'spent_gas'       => \filter_input(INPUT_POST, 'spent_gas', FILTER_SANITIZE_NUMBER_INT),
            ];

            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $rentId = $rentModel->add($addData);

            if(!$rentId){
                $this->set('message', 'Nije uspesna rezervacija');
                return;
            }

            $this->redirect( \Configuration::BASE . 'admin/rents');
        }

        public function getEdit($rentId){
            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $rent = $rentModel->getById($rentId);

            if(!$rent){
                $this->redirect( \Configuration::BASE . 'admin/rents');
                return;
            }

            $rent->delivered_at = str_replace(' ', 'T', substr($rent->delivered_at, 0, 16));
            $rent->returned_at = str_replace(' ', 'T', substr($rent->returned_at, 0, 16));
            $rent->returned_due_at = str_replace(' ', 'T', substr($rent->returned_due_at, 0, 16));

            $this->set('rent', $rent);

            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicles = $vehicleModel->getAll();
            $this->set('vehicles', $vehicles);
        }

        public function postEdit($rentId) {
            $this->getEdit($rentId);

            $editData = [
                'delivery_place'  => \filter_input(INPUT_POST, 'delivery_place', FILTER_SANITIZE_STRING),
                'return_place'    => \filter_input(INPUT_POST, 'return_place', FILTER_SANITIZE_STRING),
                'delivered_at'    => \filter_input(INPUT_POST, 'delivered_at', FILTER_SANITIZE_STRING),
                'returned_due_at' => \filter_input(INPUT_POST, 'returned_due_at', FILTER_SANITIZE_STRING),
                'returned_at'     => \filter_input(INPUT_POST, 'returned_at', FILTER_SANITIZE_STRING),
                #'user_id'         => \filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_NUMBER_INT),
                'vehicle_id'      => \filter_input(INPUT_POST, 'vehicle_id', FILTER_SANITIZE_NUMBER_INT),
                'milage'          => \filter_input(INPUT_POST, 'milage', FILTER_SANITIZE_NUMBER_INT),
                'spent_gas'       => \filter_input(INPUT_POST, 'spent_gas', FILTER_SANITIZE_NUMBER_INT),
            ];

            $rentModel = new \App\Models\RentModel($this->getDatabaseConnection());
            $res = $rentModel ->editById($rentId, $editData);

            if(!$res){
                $this->set('message', 'Nije bilo moguce izmeniti rezervaciju.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/rents');
        }
    }