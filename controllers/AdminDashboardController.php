<?php
    namespace App\Controllers;

    class AdminDashboardController extends \App\Core\Role\AdminRoleController {
        public function index() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }
    }
