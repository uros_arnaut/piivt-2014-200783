<?php

    namespace App\Controllers;

    class ApiVehicleController extends \App\Core\ApiController{
        public function show($id){
            $vehicleModel = new \App\Models\VehicleModel($this->getDatabaseConnection());
            $vehicle = $vehicleModel->getAById($id);

            $this->set('vehicle', $vehicle);
        }
    }