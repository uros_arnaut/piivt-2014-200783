<?php
    return [
        #LOGIN + REGISTER USER
        App\Core\Route::get('|^user/register/?$|',                      'Main',                      'getRegister'),
        App\Core\Route::post('|^user/register/?$|',                     'Main',                      'postRegister'),
        App\Core\Route::get('|^user/login/?$|',                         'Main',                      'getLogin'),
        App\Core\Route::post('|^user/login/?$|',                        'Main',                      'postLogin'),
        App\Core\Route::get('|^user/logout/?$|',                        'Main',                      'getLogout'),
        
        #LOGIN + REGISTER ADMIN
        App\Core\Route::get('|^admin/register/?$|',                     'Main',                      'getAdminRegister'),
        App\Core\Route::post('|^admin/register/?$|',                    'Main',                      'postRegisterAdmin'),
        App\Core\Route::get('|^admin/login/?$|',                        'Main',                      'getLoginAdmin'),
        App\Core\Route::post('|^admin/login/?$|',                       'Main',                      'postLoginAdmin'),
        App\Core\Route::get('|^admin/logout/?$|',                       'Main',                      'getLogoutAdmin'),

        App\Core\Route::get('|^categories/?$|',                         'Category',                  'categories'),
        App\Core\Route::get('|^category/([0-9]+)/?$|',                  'Category',                  'show'),
        App\Core\Route::get('|^category/([0-9]+)/delete/?$|',           'Category',                  'delete'),

        App\Core\Route::get('|^vehicle/([0-9]+)/?$|',                   'Vehicle',                   'show'),
        App\Core\Route::get('|^vehicles/?$|',                           'Vehicle',                   'vehicles'),

        App\Core\Route::get('|^contact/?$|',                            'Main',                      'getContact'),
        App\Core\Route::post('|^contact/?$|',                           'Main',                      'postContact'),
    

        App\Core\Route::post('|^search/?$|',                            'Category',                  'postSearch'),

        App\Core\Route::post('|^search/?$|',                            'AdminUserManagement',       'postSearch'),
        #API routes
        App\Core\Route::get('|^api/vehicle/([0-9]+)/?$|',               'ApiVehicle',                'show'),
        
        # --------

        # User role routes:
        App\Core\Route::get('|^user/profile/?$|',                              'UserDashboard',             'index'),
        
        #LIST
        App\Core\Route::get('|^user/rents/?$|',                                'UserRentManagement',        'rents'),
        #EDIT FORM
        App\Core\Route::get('|^user/rents/edit/([0-9]+)/?$|',                  'UserRentManagement',        'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^user/rents/edit/([0-9]+)/?$|',                 'UserRentManagement',        'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^user/rents/add/?$|',                            'UserRentManagement',        'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^user/rents/add/?$|',                           'UserRentManagement',        'postAdd'),

        # --------

        # Admin role routes:
        App\Core\Route::get('|^admin/profile/?$|',                             'AdminDashboard',            'index'),
        
        #LIST CATEGORY MANAGEMENT
        App\Core\Route::get('|^admin/categories/?$|',                          'AdminCategoryManagement',   'categories'),
        #EDIT FORM
        App\Core\Route::get('|^admin/categories/edit/([0-9]+)/?$|',            'AdminCategoryManagement',   'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/categories/edit/([0-9]+)/?$|',           'AdminCategoryManagement',   'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/categories/add/?$|',                      'AdminCategoryManagement',   'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/categories/add/?$|',                     'AdminCategoryManagement',   'postAdd'),


        #LIST VEHICLES MANAGEMENT
        App\Core\Route::get('|^admin/vehicle/([0-9]+)/?$|',                    'AdminVehicleManagement',    'show'),

        App\Core\Route::get('|^admin/vehicles/?$|',                            'AdminVehicleManagement',    'vehicles'),
        App\Core\Route::get('|^admin/vehicle/([0-9]+)/registrations/?$|',      'AdminVehicleManagement',    'registrations'),
        #EDIT FORM
        App\Core\Route::get('|^admin/vehicles/edit/([0-9]+)/?$|',              'AdminVehicleManagement',    'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/vehicles/edit/([0-9]+)/?$|',             'AdminVehicleManagement',    'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/vehicles/add/?$|',                        'AdminVehicleManagement',    'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/vehicles/add/?$|',                       'AdminVehicleManagement',    'postAdd'),
        
        App\Core\Route::get('|^admin/vehicle/([0-9]+)/registration/([0-9]+)/add/?$|',     'AdminVehicleManagement',  'getAddReg'),
        App\Core\Route::post('|^admin/vehicle/([0-9]+)/registration/([0-9]+)/add/?$|',    'AdminVehicleManagement',  'postAddReg'),

        App\Core\Route::get('|^admin/vehicle/([0-9]+)/registration/([0-9]+)/edit/?$|',    'AdminVehicleManagement',  'getEditReg'),
        App\Core\Route::post('|^admin/vehicle/([0-9]+)/registration/([0-9]+)/edit/?$|',   'AdminVehicleManagement',  'postEditReg'),


        #LIST RENTS MANAGEMENT
        App\Core\Route::get('|^admin/rents/?$|',                               'AdminRentManagement',       'rents'),
        #EDIT FORM
        App\Core\Route::get('|^admin/rents/edit/([0-9]+)/?$|',                 'AdminRentManagement',       'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/rents/edit/([0-9]+)/?$|',                'AdminRentManagement',       'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/rents/add/?$|',                           'AdminRentManagement',       'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/rents/add/?$|',                          'AdminRentManagement',       'postAdd'),


        #ADMIN USER LIST
        App\Core\Route::get('|^admin/users/?$|',                                'AdminUserManagement',       'users'),

        #LIST EXPENSES MANAGEMENT
        App\Core\Route::get('|^admin/expenses/?$|',                            'AdminExpenseManagement',       'expenses'),
        #EDIT FORM
        App\Core\Route::get('|^admin/expenses/edit/([0-9]+)/?$|',              'AdminExpenseManagement',       'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/expenses/edit/([0-9]+)/?$|',             'AdminExpenseManagement',       'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/expenses/add/?$|',                        'AdminExpenseManagement',       'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/expenses/add/?$|',                       'AdminExpenseManagement',       'postAdd'),


        #LIST FEATURES MANAGEMENT
        App\Core\Route::get('|^admin/features/?$|',                            'AdminFeatureManagement',       'features'),
        #EDIT FORM
        App\Core\Route::get('|^admin/features/edit/([0-9]+)/?$|',              'AdminFeatureManagement',       'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/features/edit/([0-9]+)/?$|',             'AdminFeatureManagement',       'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/features/add/?$|',                        'AdminFeatureManagement',       'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/features/add/?$|',                       'AdminFeatureManagement',       'postAdd'),


        #LIST PRICE MANAGEMENT
        App\Core\Route::get('|^admin/prices/?$|',                              'AdminPriceManagement',         'prices'),
        #EDIT FORM
        App\Core\Route::get('|^admin/prices/edit/([0-9]+)/?$|',                'AdminPriceManagement',         'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/prices/edit/([0-9]+)/?$|',               'AdminPriceManagement',         'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/prices/add/?$|',                          'AdminPriceManagement',         'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/prices/add/?$|',                         'AdminPriceManagement',         'postAdd'),


        #LIST REGISTRATION MANAGEMENT
        App\Core\Route::get('|^admin/registrations/?$|',                       'AdminRegistrationManagement',       'registrations'),
        #EDIT FORM
        App\Core\Route::get('|^admin/registrations/edit/([0-9]+)/?$|',         'AdminRegistrationManagement',       'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/registrations/edit/([0-9]+)/?$|',        'AdminRegistrationManagement',       'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/registrations/add/?$|',                   'AdminRegistrationManagement',       'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/registrations/add/?$|',                  'AdminRegistrationManagement',       'postAdd'),


        #LIST VEHICLE_FEATURE MANAGEMENT
        App\Core\Route::get('|^admin/vehicleFeatures/?$|',                     'AdminVehicleFeatureManagement',      'vehicleFeatures'),
        #EDIT FORM
        App\Core\Route::get('|^admin/vehicleFeatures/edit/([0-9]+)/?$|',       'AdminVehicleFeatureManagement',      'getEdit'),
        #EDIT LOGIC
        App\Core\Route::post('|^admin/vehicleFeatures/edit/([0-9]+)/?$|',      'AdminVehicleFeatureManagement',      'postEdit'),
        #ADD FORM
        App\Core\Route::get('|^admin/vehicleFeatures/add/?$|',                 'AdminVehicleFeatureManagement',      'getAdd'),
        #ADD LOGIC
        App\Core\Route::post('|^admin/vehicleFeatures/add/?$|',                'AdminVehicleFeatureManagement',      'postAdd'),

        # --------
        
        #fallback route
        App\Core\Route::any('|^.*$|',                                          'Main',                               'home'),
    ];