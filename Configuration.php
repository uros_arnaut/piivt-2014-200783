<?php
    final class Configuration{
        const BASE = 'http://localhost/project/';

        const DATABASE_HOST = 'localhost';
        const DATABASE_USER = 'root';
        const DATABASE_PASS = '';
        const DATABASE_NAME = 'piivt';

	    const SESSION_STORAGE = '\\App\\Core\\Session\\FileSessionStorage';
        const SESSION_STORAGE_DATA = [ './sessions/' ];
        const SESSION_LIFETIME = 3600;

        const FINGERPRINT_PROVIDER_FACTORY = '\\App\\Core\\Fingerprint\\BasicFingerprintProviderFactory';
        const FINGERPRINT_PROVIDER_METHOD  = 'getInstance';
        const FINGERPRINT_PROVIDER_ARGS    = [ 'SERVER' ];

        const UPLOAD_DIR = 'assets/uploads/';

        const MAIL_HOST     = 'smtp.office365.com'; #localhost
        const MAIL_PORT     = '587'; #port koji odgovara TLS-u
        const MAIL_PROTOCOL = 'tls';
        const MAIL_USERNAME = ''; #test@test.com
        const MAIL_PASSWORD = ''; #test123
    
        const ADMIN_MAIL = '';
    }